﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace TelegramXAMLMarkUpWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private User _currentUser, _user;

        public MainWindow()
        {
            InitializeComponent();

            _currentUser = new User
            {
                Name = "A person",
                Image = "https://i.gyazo.com/bcf82c05823aa921df1ab0510c32e64b.png",
            };

            currentUserImage.Fill = new ImageBrush { ImageSource = (ImageSource)new ImageSourceConverter().ConvertFromString(_currentUser.Image) };

            _user = new User
            {
                Name = "Worst nightmare...",
                Image = "https://www.shareicon.net/download/2015/10/27/662471_people_512x512.png",
                Messages = new List<string> { "Howdy!", "Don't ignore meee" }
            };

            anotherUserImage.Fill = new ImageBrush { ImageSource = (ImageSource)new ImageSourceConverter().ConvertFromString(_user.Image) };

            anotherUserName.Text = _user.Name;

            usersTreeView.Items.Add(
                new
                {
                    _user.Name,
                    _user.Image,
                    Message = _user.Messages[_user.Messages.Count - 1]
                });

            foreach (var message in _user.Messages)
            {
                usersMessagesTreeView.Items.Add(
                    new
                    {
                        _user.Name,
                        _user.Image,
                        Message = message
                    });
            }
        }

        private void SearchNameBoxTextChanged(object sender, TextChangedEventArgs e)
        {
            foreach (var item in usersTreeView.Items)
            {
                ((TreeViewItem)usersTreeView.ItemContainerGenerator.ContainerFromItem(item)).Visibility = Visibility.Collapsed;
            }

            if (((TextBox)sender).Text == string.Empty)
            {
                foreach (var item in usersTreeView.Items)
                {
                    ((TreeViewItem)usersTreeView.ItemContainerGenerator.ContainerFromItem(item)).Visibility = Visibility.Visible;
                }
            }
            else
            {
                foreach (var item in usersTreeView.Items)
                {
                    if (item.ToString().Contains(((TextBox)sender).Text))
                    {
                        ((TreeViewItem)usersTreeView.ItemContainerGenerator.ContainerFromItem(item)).Visibility = Visibility.Visible;
                    }
                }
            }

        }

        private void DefaultStyleButtonClick(object sender, RoutedEventArgs e)
        {
            header.Style = (Style)FindResource("DefaultTheme");
            continuedHeader.Style = (Style)FindResource("DefaultTheme");
        }

        private void DarkStyleButtonClick(object sender, RoutedEventArgs e)
        {
            header.Style = (Style)FindResource("DarkTheme");
            continuedHeader.Style = (Style)FindResource("DarkTheme");
        }

        private void LightStyleButtonClick(object sender, RoutedEventArgs e)
        {
            header.Style = (Style)FindResource("LightTheme");
            continuedHeader.Style = (Style)FindResource("LightTheme");
        }

        private void SendMessageButtonClick(object sender, RoutedEventArgs e)
        {
            if (messageBox.Text != "")
            {
                _user.Messages.Add(messageBox.Text);

                usersMessagesTreeView.Items.Add(
                    new
                    {
                        _currentUser.Name,
                        _currentUser.Image,
                        Message = messageBox.Text,
                    });

                messageBox.Clear();
            }
            else
            {
                MessageBox.Show("Enter a message");
            }
        }
    }
}
